from distutils.core import setup
import json

with open('package.json', 'r') as f:
    package_json = json.load(f)

version = package_json['version']

setup(
    name = 'scylla-netsuite',
    packages = ['scylla_netsuite'],
    version = version,

    description = 'Scylla-NetSuite connects Scylla with a NetSuite account for further integration.',

    #author = '',
    #author_email = '',

    url = 'https://git@gitlab.com:5stones/scylla-netsuite',
    download_url = 'https://gitlab.com/5stones/scylla-netsuite/repository/archive.tar.gz?ref=' + version,

    keywords = 'integration',

    classifiers=[
        #'Development Status :: 4 - Beta',
        #'Development Status :: 5 - Production/Stable',

        #'Intended Audience :: Developers',
        #'Topic :: Software Development :: Build Tools',

        'License :: OSI Approved :: MIT License',

        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.7',
    ],

    install_requires = [
        'scylla',
    ],
    dependency_links=[
        'git+https://gitlab.com:5stones/scylla.git',
        'requests[security]',
        'requests_oauthlib',
    ],
)
