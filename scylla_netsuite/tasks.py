import scylla
from scylla import orientdb
from . import client
from . import records
from . import convert


class DownloadTask(scylla.Task):
    """Download a record type from NetSuite"""

    timing_buffer = 5

    def __init__(self, ns_type, searchid=None, track_updates=True, limit=100, task_id=None):
        if not task_id:
            task_id = "Ns{}-download".format(ns_type.title())
        super(DownloadTask, self).__init__(task_id)

        self.ns_type = ns_type
        self.searchid = searchid
        self.track_updates = track_updates
        self.limit = limit

    def _step(self, after, options):
        """ Downloads records from netsuite that have been updated or created since the last time this function was called.
        """
        args = {
            'searchid': self.searchid,
            'limit': self.limit,
            'offset': options.get('offset', 0),
        }
        limit = options.get('limit', None)
        if limit:
            args['limit'] = limit

        timestamp_type = self.timestamp_type
        if after:
            timestamp_field = '{0}after'.format(timestamp_type)
            args[timestamp_field] = convert.to_netsuite_date(after)

        print("Downloading {0} records {1} after {2} :".format(
            self.ns_type, timestamp_type, after))
        self.search_records(**args)

    @property
    def timestamp_type(self):
        if self.track_updates:
            return 'modified'
        else:
            return 'created'

    def _process_result(self, result):
        """
        Process a netsuite json object and save it to the database.
        """
        ns_obj = records.NsRecord(result)
        with scylla.ErrorLogging(self.task_id, ns_obj):
            ns_obj.throw_at_orient()
            print("Saved {0} {1} as {2} {3}".format(
                result['recordtype'], result['id'], ns_obj.classname, ns_obj.rid))

    def process_one(self, internalid, ns_type=None):
        """
        Get a netsuite record and save it to orientdb.
        """
        if not ns_type or self.ns_type[:13].lower() == 'customrecord_':
            ns_type = self.ns_type
        result = client.get_record(ns_type, internalid)
        self._process_result(result)

    def search_records(self, searchid=None, createdafter=None, modifiedafter=None, offset=0, limit=100):
        """ Runs a netsuite search and saves all records to the database.
        """
        offset = int(offset)
        limit = int(limit)
        total = '?'
        while total == '?' or offset < total:
            print("\nLoading {0}s {1}-{2} of {3}...".format(
                self.ns_type, offset, offset+limit-1, total))
            response = client.search(self.ns_type, searchid=searchid, createdafter=createdafter, modifiedafter=modifiedafter, limit=limit, offset=offset)
            for result in response['results']:
                self._process_result(result)
            offset += limit
            total = int(response['total'])
        print("\nFinished saving {0} {1}s.\n".format(
            total, self.ns_type))

    def retry_record_error(self, error):
        (record_type, record_id) = error['document_ref']
        self.process_one(record_id, ns_type=record_type[2:])


class UpdateInventoryTask(DownloadTask):
    """Special task to pull inventory items that have recent transactions"""

    # Netsuite record types that affect inventory
    transaction_types = [
        'NsCashrefund',
        'NsInventoryadjustment',
        'NsInventorystatuschange',
        'NsInventorytransfer',
        'NsItemfulfillment',
        'NsItemreceipt',
        'NsPurchaseorder',
        'NsSalesorder',
        'NsStorepickupfulfillment',
        'NsTransferorder',
    ]

    def __init__(self):
        super(UpdateInventoryTask, self).__init__(
            'inventoryitem', task_id="NsInventoryitem-update")

    def _step(self, after, options):
        if not after:
            # download all inventory items
            super(UpdateInventoryTask, self)._step(after, options)
            return

        print("Finding items in NsTransactions after {}:".format(after))
        ids = self._query_ids(after)

        if ids:
            print("Downloading {} {}s:".format(len(ids), self.ns_type))
            self.process_ids(ids)

    def _query_ids(self, after):
        """Get item ids in recent transactions"""
        query = (
            'SELECT set(set(item.item.internalid, inventory.item.internalid)) AS ids, '
            '  set(item.itemname) AS skus '
            'FROM NsTransaction '
            'WHERE _updated_at > {} '
            '  and @class in {}')
        result = orientdb.execute(query.format(
            orientdb.encode(after),
            orientdb.encode(self.transaction_types)))
        try:
            return self._filter_ids(result[0]['ids'], result[0]['skus'])
        except (IndexError, KeyError):
            return []

    @staticmethod
    def _filter_ids(ids, skus):
        """Filter item ids to just inventoryitem ids"""
        query = (
            'SELECT set(id) AS ids '
            'FROM NsInventoryitem '
            'WHERE id in {} or itemid in {}')
        result = orientdb.execute(query.format(
            orientdb.encode(ids),
            orientdb.encode(skus)))
        return result[0]['ids']


class ToNetsuiteTask(scylla.ReflectTask):
    """Checks for updates to records an pushes them into netsuite.
    """
    link_field = 'id'

    def __init__(self, from_class, conversion, to_type, where=None, with_reflection=None):
        super(ToNetsuiteTask, self).__init__(
            from_class,
            ('Ns', to_type),
            where=where,
            with_reflection=with_reflection)
        self.conversion = conversion

    def _process_response_record(self, obj):
        """Uploads the record to netsuite.
        """
        data = self.conversion(obj)

        if data is not None:
            # create the record in netsuite
            #print('Upload to netsuite:')
            #print(json.dumps(data, indent=2))
            is_update = data.get('id', False)
            with scylla.Uninterruptable():
                ns_result = client.save(data)

                # save the netsuite response to orient and link it
                ns_obj = records.NsRecord(ns_result)
                self._save_response(obj, ns_obj, is_update, request=data)
