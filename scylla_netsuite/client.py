import json
import requests
import scylla
import requests_oauthlib


__config = scylla.configuration.getSection('Netsuite', {
    'debug': False,
})


def __get_options():
    options = {
        'headers': {'Content-Type': 'application/json'},
    }
    if 'oauth' in __config:
        options['auth'] = requests_oauthlib.OAuth1(**__config['oauth'])
    elif 'password' in __config:
        options['headers']['Authorization'] = (
            'NLAuth '
            'nlauth_account={account}, '
            'nlauth_email={email}, '
            'nlauth_signature={password}, '
            'nlauth_role={role}'
        ).format(**__config)
    else:
        raise Exception('Config of password or oauth required')

    return options


class NetsuiteError(Exception):
    def __init__(self, response, method='', params=None):
        try:
            response_obj = response.json()

            if 'error' in response_obj:
                self.message = '{code}: {message}'.format(**response_obj['error'])
        except ValueError:
            response_obj = response.text

        if not self.message:
            self.message = response.text

        super(NetsuiteError, self).__init__({
            'response': (response.status_code, response_obj),
            'request': (method, params),
        })

    def __str__(self):
        return self.message


def __process_response(response, method='', params=None):
    if response.status_code != 200:
        raise NetsuiteError(response, method=method, params=params)
    if __config.get('debug'):
        print('NetSuite response: ' + json.dumps(response.json(), default=str, indent=2))
    return response.json()


def get(params):
    response = requests.get(__config['url'], params=params, **__get_options())
    return __process_response(response, 'GET', params)

def post(data):
    json_data = json.dumps(data, default=str)
    if __config.get('debug'):
        print('NetSuite POST: ' + json.dumps(data, default=str, indent=2))
    response = requests.post(__config['url'], data=json_data, **__get_options())
    return __process_response(response, 'POST', data)

def put(data):
    json_data = json.dumps(data, default=str)
    if __config.get('debug'):
        print('NetSuite PUT: ' + json.dumps(data, default=str, indent=2))
    response = requests.put(__config['url'], data=json_data, **__get_options())
    return __process_response(response, 'PUT', data)

def delete(params):
    response = requests.delete(__config['url'], params=params, **__get_options())
    return __process_response(response, 'DELETE', params)


def save(data):
    """Saves the data to netsuite and assumes, the recordtype and id is already in data.
    """
    if data.get('id'):
        return put(data)
    else:
        return post(data)


def get_record(recordtype, internalid):
    return get( {
        'recordtype': recordtype,
        'internalid': internalid
    } )

def search(recordtype, searchid=None, createdafter=None, modifiedafter=None, offset=0, limit=100):
    return get(locals())

def update_record(recordtype, internalid, data):
    data['recordtype'] = recordtype
    data['id'] = internalid
    return put(data)

def create_record(recordtype, data):
    data['recordtype'] = recordtype
    return post(data)

def create_record_from(recordtype, from_recordtype, from_id, data):
    data['recordtype'] = recordtype
    data['createfrom'] = {
        'recordtype': from_recordtype,
        'id': from_id
    }
    return post(data)

def merge(recordtype, internalid, merge_in):
    return put( {
        'recordtype': recordtype,
        'id': internalid,
        'merge_in': merge_in
    } )

def set_inactive(recordtype, internalid):
    return delete( {
        'recordtype': recordtype,
        'id': internalid
    } )

