from scylla import app
from scylla import configuration
from . import tasks


class App(app.App):

    main_options = [
        ('after', 'a', None),
        ('limit', 'l', None),
        ('offset', 'o', 0),
        ('retry-errors', 'r'),
    ]

    def _prepare(self):
        self.on_demand_tasks['update-inventory'] = tasks.UpdateInventoryTask()

        for task in configuration.get('Netsuite', 'tasks', []):
            if task[0] in self.on_demand_tasks:
                self.tasks[task[0]] = self.on_demand_tasks[task[0]]
            else:
                self.tasks[task[0]] = tasks.DownloadTask(*task)

    def get_task(self, record_type):
        """get a task for an argument"""
        try:
            return super(App, self).get_task(record_type)
        except KeyError:
            return tasks.DownloadTask(record_type)
