from scylla import configuration
from scylla import orientdb
from scylla import records
from . import client
from . import convert

class NsRecord(records.Record):
    classname = 'NsRecord'

    config = configuration.getSection('Netsuite', {
        'datetime_fields': ['datecreated', 'createddate', 'lastmodifieddate' ],
        'date_fields': [
            'trandate', 'saleseffectivedate', 'expectedclosedate',
            'startdate', 'duedate', 'billdate', 'shipdate',
        ],
    })

    movable_classes = {
        'inventoryitem': 'item',
    }

    _flatten_fields = {
        "_created_from_id": ('createdfrom', 'internalid'),
    }

    def __init__(self, data):
        super(NsRecord, self).__init__(data)
        self.classname = self.__format_classname(data['recordtype'])

        # parse id type
        self.data['id'] = int(self.data['id'])
        # parse dates and datetimes
        self.__parse_date_fields(self.data)
        self.__parse_flatten_fields(self.data)

    @staticmethod
    def __format_classname(record_type):
        if record_type.startswith('customrecord_'):
            record_type = record_type[13:]
        return 'Ns' + record_type.title().replace('_','')

    @classmethod
    def __parse_date_fields(cls, data):
        """Convert datetime strings to datetime objects."""
        cls.__parse_fields(data, cls.config['datetime_fields'], convert.parse_netsuite_datetime)
        cls.__parse_fields(data, cls.config['date_fields'], convert.parse_netsuite_date)

    @classmethod
    def __parse_fields(cls, data, fields, func):
        """Recursively parses fields using a parsing function."""
        for (field, value) in data.items():
            if isinstance(value, dict):
                cls.__parse_fields(value, fields, func)
            elif isinstance(value, list) and len(value) and isinstance(value[0], dict):
                for v in value:
                    cls.__parse_fields(v, fields, func)
            elif field in fields:
                data[field] = func(value)

    @classmethod
    def __parse_flatten_fields(cls, data):
        for key, val in cls._flatten_fields.iteritems():
            data[key] = cls._get_nested_value(data, val);

    @classmethod
    def _get_nested_value(cls, obj, path):
        key = path[0]
        path = path[1:]

        if key not in obj:
            return None
        elif len(path) > 0:
            return cls._get_nested_value(obj[key], path)
        else:
            return obj[key]

    def throw_at_orient(self):
        """save to orientdb, moving classes if necessary"""
        try:
            return super(NsRecord, self).throw_at_orient()
        except orientdb.OrientDbError as err:
            if self.__move_existing():
                # reattempt after moving record
                return super(NsRecord, self).throw_at_orient()
            raise err

    def __move_existing(self):
        """attempt to move a record before updating it"""
        try:
            old_type = self.movable_classes[self['recordtype']]
            old_classname = self.__format_classname(old_type)
        except KeyError:
            return False

        old_query = 'SELECT FROM {} WHERE id = {} AND @class != "{}" LIMIT 1'
        old_rec = old_query.format(
            old_classname, self['id'], self.classname)

        result = orientdb.execute('MOVE VERTEX ({}) TO CLASS:{}'.format(
            old_rec, self.classname))

        return len(result) > 0


class ConnectedRecord(records.Record):
    """Manages a record which gets uploaded to Netsuite.

    Attributes:
        nsid: The netsuite internal id, once we know it's linked up.
        has_ns_edge: Whether or not it's linked to netsuite in the database. Uses
            None if we don't know yet.
        ns_conversion: The Conversion object used to convert the Vindicia obj
            to an object which will be passed to netsuite to update or create
            a matching record.
        ns_update_fields: A list of fields to update when a record is updated instead of created.
    """
    ns_update_fields = None
    ns_conversion = None
    ns_upsert_fields = None

    def __init__(self, obj):
        super(ConnectedRecord, self).__init__(obj)
        self.nsid = None
        self.has_ns_edge = None

    def query_nsid(self):
        # make sure it's in orient
        if not self.rid:
            self.throw_at_orient()

        # check for linked netsuite record
        if not self.nsid:
            sql = "SELECT last(both('Reflection').id) AS nsid FROM "+self.rid
            result = orientdb.execute(sql)
            if len(result):
                self.nsid = result[0].get('nsid')
                self.has_ns_edge = True
            else:
                self.has_ns_edge = False

        return self.nsid

    def to_netsuite_obj(self):
        """Convert this to netsuite data using a Conversion obj.
        """
        if not self.ns_conversion:
            raise Exception("No netsuite conversion for {0}.".format(self.classname))

        if self.nsid:
            return self.ns_conversion.convert(self.data, self.ns_update_fields)
        else:
            return self.ns_conversion(self.data)

    def _upload_to_netsuite(self, extra_data=None):
        """Uploads the record to netsuite, with optional extra fields to set, and returns the NsRecord.
        """
        ns_data = self.to_netsuite_obj()
        if extra_data:
            ns_data.update(extra_data)

        # create the record in netsuite
        #print('Upload to netsuite:')
        #print(json.dumps(ns_data, indent=2))
        if self.nsid:
            ns_data['id'] = self.nsid
            ns_result = client.put(ns_data)
        else:
            if self.ns_upsert_fields:
                ns_data['upsertfields'] = self.ns_upsert_fields
            ns_result = client.post(ns_data)
        self.nsid = ns_result['id']

        # save the netsuite response to orient and link it
        ns_obj = NsRecord(ns_result)
        ns_obj.throw_at_orient()
        if self.has_ns_edge == False:
            orientdb.create_edge(self.rid, ns_obj.rid, 'Created', content={'request':ns_data})
        else:
            orientdb.create_edge(self.rid, ns_obj.rid, 'Updated', content={'request':ns_data})

        return ns_obj

    def update_netsuite(self):
        self.query_nsid()
        if not self._should_update_netsuite():
            return None
        return self._upload_to_netsuite()

    def _should_update_netsuite(self):
        return True
