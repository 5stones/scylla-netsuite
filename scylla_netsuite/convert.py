from datetime import datetime, tzinfo
from scylla import configuration
from scylla import convert


__config = configuration.getSection('Netsuite')


class Conversion(convert.Conversion):
    id_field = '_linked_to'

    update_fields = None
    upsert_fields = None

    def __init__(self,
            conversion_map=None,
            post_convert=None,
            update_fields=None,
            upsert_fields=None):

        super(Conversion, self).__init__(conversion_map, post_convert)

        if update_fields:
            self.update_fields = update_fields
        if upsert_fields:
            self.upsert_fields = upsert_fields
        if 'id' not in self.conversion_map:
            self.add_conversion('id', self.id_field)

    def convert(self, obj, included_fields=None):
        if included_fields is None and obj.get(self.id_field):
            included_fields = self.update_fields

        data = super(Conversion, self).convert(obj, included_fields)

        if 'upsert_fields' not in data and not data.get('id'):
            data['upsertfields'] = self.upsert_fields

        return data


def parse_netsuite_date(value):
    """Parse a netsuite formated date into a date object."""
    if value is None:
        return None
    return datetime.strptime(value, __config['date_format']).date()


def parse_netsuite_datetime(value):
    """Parse a netsuite formated datetime into a datetime object."""
    if value is None:
        return None
    for f in __config['datetime_format']:
        try:
            d = datetime.strptime(value, f)
            return d.replace(tzinfo=convert.FixedTz(__config['timezone']))
        except ValueError:
            pass
    return parse_netsuite_date(value)


def to_netsuite_date(value):
    """Change a datetime into a Netsuite style datetime."""
    if value is None:
        return None
    if not isinstance(value, datetime):
        value = convert.parse_iso_date(value)
    if value.tzinfo:
        fixed_tz = convert.FixedTz(__config['timezone'])
        value = value.astimezone(fixed_tz)
    return value.strftime(__config['datetime_format'][0])


def parse_datetime(d):
    """Parses an ISO 8601 datetime or Netsuite datetime string into a datetime object."""
    d_datetime = convert.parse_iso_date(d)
    if d_datetime is not None:
        return d_datetime
    return parse_netsuite_datetime(d)


def __main():
    # test ns parsing
    for s in ['2/6/2015 14:09', '02/06/2015 2:00:01']:
        print(s)
        d = parse_datetime(s)
        print("iso: {0}".format(d))
        print("ns: {0}".format(to_netsuite_date(d)))
        print("")


if __name__ == '__main__':
    __main()

