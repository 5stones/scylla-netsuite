"""Scylla module to integrate with NetSuite.
Includes app for pulling records from NetSuite, as well as utilities to push records into NetSuite.

configuration required:
    'Netsuite': {
        'url': 'https://rest.sandbox.netsuite.com/app/site/hosting/restlet.nl?script=...&deploy=...',

        # nlauth login
        'account': '',
        'email': '',
        'password': '',
        'role': '',

        # oauth login
        'oauth': {
            'realm': '',  # account id (with _SB1 for sandbox)
            'client_key': '',
            'client_secret': '',
            'resource_owner_key': '',
            'resource_owner_secret': ''
        },

        'date_format': '%m/%d/%Y',
        #'datetime_format': ('%m/%d/%Y %I:%M:%S %p', '%m/%d/%Y %I:%M %p'),
        'datetime_format': ('%m/%d/%Y %H:%M:%S', '%m/%d/%Y %H:%M'),
        'timezone': '+00',

        'datetime_fields': ['datecreated', 'createddate', 'lastmodifieddate' ],
        'date_fields': [
            'trandate', 'saleseffectivedate', 'expectedclosedate',
            'startdate', 'duedate', 'billdate', 'shipdate',
        ],

        'tasks': [
          ('customer', 'customsearch_scylla_customer_sync'),
          ('partner',),
          ('item',),
          ('salesorder',),
          ('transaction',),
        ],
    },
"""

from .client import *

from .app import App

from .tasks import ToNetsuiteTask

from .records import NsRecord, ConnectedRecord

from .convert import *
