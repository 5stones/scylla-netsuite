## [1.1.1](https://gitlab.com/5stones/scylla-netsuite/compare/v1.1.0...v1.1.1) (2019-05-03)


### Bug Fixes

* **NsRecord:** Fix issues with field flattening ([38d448c](https://gitlab.com/5stones/scylla-netsuite/commit/38d448c))



# [1.1.0](https://gitlab.com/5stones/scylla-netsuite/compare/v1.0.0...v1.1.0) (2019-05-03)


### Bug Fixes

* **inventory:** fix updating inventory for ItemReceipts ([5a54cbf](https://gitlab.com/5stones/scylla-netsuite/commit/5a54cbf))



# [1.0.0](https://gitlab.com/5stones/scylla-netsuite/compare/95501cf...v1.0.0) (2019-02-19)


### Bug Fixes

* fix syntax errors from refactor ([95501cf](https://gitlab.com/5stones/scylla-netsuite/commit/95501cf))
* **Conversion:** fix upsert fields ([4725a03](https://gitlab.com/5stones/scylla-netsuite/commit/4725a03))
* **convert:** fix constructor when conversion_map is None ([03363c3](https://gitlab.com/5stones/scylla-netsuite/commit/03363c3))
* **records:** support conversion into inventoryitems ([14455eb](https://gitlab.com/5stones/scylla-netsuite/commit/14455eb))
* **tasks:** fix KeyError and task_id for UpdateInventory ([38930bb](https://gitlab.com/5stones/scylla-netsuite/commit/38930bb))
* **tasks:** fix retrying errors for custom records ([ff6cf78](https://gitlab.com/5stones/scylla-netsuite/commit/ff6cf78))
* **tasks:** fix retrying of errors ([236f147](https://gitlab.com/5stones/scylla-netsuite/commit/236f147))


### Features

* **client:** support oauth login with tokens ([3b8c9c7](https://gitlab.com/5stones/scylla-netsuite/commit/3b8c9c7))
* **tasks:** create task to update inventory when transactions are updated ([ffeb8c7](https://gitlab.com/5stones/scylla-netsuite/commit/ffeb8c7))
* **tasks:** prevent shutdown between sending something to NetSuite and saving the response ([f99007c](https://gitlab.com/5stones/scylla-netsuite/commit/f99007c))
* **tasks:** skip if None ([4678f8b](https://gitlab.com/5stones/scylla-netsuite/commit/4678f8b))



